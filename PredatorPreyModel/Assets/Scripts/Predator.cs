using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Predator : MonoBehaviour
{

    public GameObject pointA;
    public GameObject pointB;
    public GameObject neighField;
    public GameObject huntField;
    public float rotSpdDegPerSec;
    public float pauseBetweenHunts = 15f;

    public bool DrawSep;
    public bool DrawMovementDir;
    public bool DrawToVictim;

    private AnimalControl _animalControlScript;

    private HashSet<GameObject> _neighbours;
    private HashSet<GameObject> _victims;


    private Vector3 _pointPos;
    private Vector3 _pointDestination;

    private Vector3 _pointA;
    private Vector3 _pointB;

    private Vector3 _vecToDestination;
    private Vector3 _vecFwd;
    private Vector3 _vecMovementDir;
    private Vector3 _vecToPrey;

    private float speed;
    private float _separation;
    private float _forwardLength;
    private float _closestDistanceToVictim;

    private bool _isHunting;
    private bool _isPursuing;

    public static class ReynoldForces
    {
        public static Vector3 Sep = Vector3.zero;
    }

    public bool IsGoingToA { get; set; } = false;
    public bool IsGoingToB { get; set; } = false;



    private void Start()
    {
        _neighbours = neighField.GetComponent<NeighField>().neighbours;
        Debug.Assert(_neighbours != null);

        _animalControlScript = GameObject.Find("AnimalControl").GetComponent<AnimalControl>();
        Debug.Assert(_animalControlScript != null);

        _victims = huntField.GetComponent<HuntField>().victims;
        Debug.Assert(_victims != null);

        _forwardLength = transform.localScale.z;
        _pointPos = transform.position;
        _pointA = pointA.transform.position;
        _pointB = pointB.transform.position;
        _pointDestination = IsGoingToA ? _pointA : _pointB;

        _isHunting = true;



    }

    private void FixedUpdate()
    {
        _pointPos = transform.position;
        _vecFwd = transform.forward;

        _pointDestination = IsGoingToA ? _pointA : _pointB;
        if (IsGoingToA && _pointDestination.Equals(_pointB)
            || IsGoingToB && _pointDestination.Equals(_pointA))
        {
            Debug.LogError("FUCK");
        }
        _vecToDestination = (_pointDestination - _pointPos).normalized;


        ComputeReynoldForces();
        ComputeHuntingVector();
        _vecMovementDir = GetResultMovement();
        RotateEntity(_vecMovementDir);
        MoveEntity();
        DebugDraw();
    }
    private void Update()
    {
        _separation = _animalControlScript.PreySeparation;
        speed = _animalControlScript.PredatorSpeed;
        rotSpdDegPerSec = _animalControlScript.PredatorRotSpd;
    }
    IEnumerator DontTakeInterestInPrey(float duration)
    {
        _isHunting = false;
        yield return new WaitForSecondsRealtime(duration);
        _isHunting = true;


    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.CompareTag("PredA"))
        {
            IsGoingToB = true;
            IsGoingToA = false;
        }
        else if (other.CompareTag("PredB"))
        {

            IsGoingToA = true;
            IsGoingToB = false;
        }
        else if (other.CompareTag("Prey"))
        {
            if(_isHunting)
                StartCoroutine(DontTakeInterestInPrey(pauseBetweenHunts));
        }
      
    }


    private void RotateEntity(Vector3 lookTo)
    {
        Quaternion targetRotation = Quaternion.LookRotation(lookTo);
        float rot = _isPursuing ? rotSpdDegPerSec * 3:rotSpdDegPerSec;
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, Time.fixedDeltaTime * rotSpdDegPerSec);

    }

    private void MoveEntity()
    {
        float spd = speed;

        if (_isPursuing && _closestDistanceToVictim < 10f)
            spd = 3 * speed;
        else if (_isPursuing)
            spd = 2 * speed;

        transform.position = _pointPos + spd * Time.fixedDeltaTime * _vecFwd;
    }
    // SocialEntity
    private void ComputeReynoldForces()
    {
        ReynoldForces.Sep = Vector3.zero;


        foreach (var n in _neighbours)
        {
            float distance = Vector3.Distance(_pointPos, n.transform.position);
            Vector3 vecToNeigh = (n.transform.position - _pointPos);
            if (distance < _separation)
            {
                Debug.Log("Computing sep for pred");
                float power = _separation / distance;
                ReynoldForces.Sep += -power * vecToNeigh;
            }
        }
        ReynoldForces.Sep.Normalize();
    }

    private float GetPowerOfVector(in Vector3 leadDirection, in Vector3 directionToAdd)
    {
        float angleRad = Mathf.Deg2Rad * Vector3.Angle(leadDirection, directionToAdd);
        float ret = Mathf.Cos(angleRad) / 2 + .5f;
        return ret;
    }

    private Vector3 GetResultMovement()
    {

        // Redo with priority decisions whne being pursued
        Vector3 result = _vecToPrey;
        result = (result + GetPowerOfVector(in result, in ReynoldForces.Sep) * ReynoldForces.Sep);
        result = (result + GetPowerOfVector(in result, in _vecToDestination) * _vecToDestination);
    

        return result.normalized;
    }

    // hunting
    private void ComputeHuntingVector()
    {
        _vecToPrey = Vector3.zero;
        Vector3 tmp = Vector3.zero;
        _isPursuing = false;

        
        _closestDistanceToVictim = float.PositiveInfinity;
        if (_isHunting)
        {
            foreach (var v in _victims)
            {

                tmp = (v.transform.position - _pointPos);
                if (tmp.magnitude < _closestDistanceToVictim)
                {
                    _closestDistanceToVictim = tmp.magnitude;
                    
                }
            }
        }

        if (!tmp.Equals(Vector3.zero))
        {
            _isPursuing = true;
            _vecToPrey = tmp.normalized;
        }
    }

    private void DebugDraw()
    {
        float l = 5f;
        if (DrawMovementDir)
            Debug.DrawLine(_pointPos, _pointPos + l * _vecMovementDir, Color.white);
        if (DrawSep)
            Debug.DrawLine(_pointPos, _pointPos + l * ReynoldForces.Sep, Color.grey);
        if (DrawToVictim)
            Debug.DrawLine(_pointPos, _pointPos + l * _vecToPrey, Color.red);
      
    }
}
