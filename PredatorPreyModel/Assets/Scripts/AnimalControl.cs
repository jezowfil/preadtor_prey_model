using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalControl : MonoBehaviour
{
    public float PreySpeed;
    public float PreyRotSpd;
    public float PreySeparation;
    public float PreyNeighbourhoodRadius;
    public float PreyPredatorNeighRadius;
    public float PreyPredatorFearRadius;
    


    public float PredatorSpeed;
    public float PredatorRotSpd;
    public float PredatorVisionRadius;
    public float PredatorHuntRadius;
}
