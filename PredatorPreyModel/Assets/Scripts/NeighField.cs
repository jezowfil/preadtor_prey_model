using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class NeighField : MonoBehaviour
{
    public GameObject AnimalControl;
    private AnimalControl _animalControlScript;

    private GameObject _parent;
    private Prey _parentPreyScript;

    private int _maxNumOfNeighbours = 20;

    private SphereCollider _collider;

    public readonly HashSet<GameObject> neighbours = new();
    
    //public readonly List<Vector3> ReynoldForces = new() { new Vector3(), new Vector3(), new Vector3() };

    private void Start()
    {
        _parent = transform.parent.gameObject;
        _parentPreyScript = _parent.transform.GetComponent<Prey>();
        
        _animalControlScript = AnimalControl.GetComponent<AnimalControl>();
        if(TryGetComponent<SphereCollider>(out SphereCollider c))
        {
            _collider = c;
        }
        else
        {
            _collider = transform.gameObject.AddComponent<SphereCollider>();
            _collider.isTrigger = true;
        }

        if(!TryGetComponent<Rigidbody>(out Rigidbody rb))
        {
            rb = transform.gameObject.AddComponent<Rigidbody>();  
        }
        rb.useGravity = false;
        rb.isKinematic = true;
        
    }
    private void Update()
    {
        
        _collider.radius = _animalControlScript.PreyNeighbourhoodRadius; 
    }

    private void OnTriggerEnter(Collider other)
    {
        if( neighbours.Count < _maxNumOfNeighbours
            && other.CompareTag(_parent.tag)
            && !other.transform.gameObject.Equals(_parent))
        {
        

            if(neighbours.Add(other.transform.gameObject) && _parent.CompareTag("Pred"))
            {
                Debug.Log($"Added {other.gameObject.name} to {_parent.name}'s neighbours.");

            }
   
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if(other.CompareTag(_parent.tag))
        {
         
            if (neighbours.Remove(other.transform.gameObject))
            {
                //Debug.Log($"Removed {other.gameObject.name} from {_parent.name} neighbours.");

            }
                

            
        }
    }
   
}
