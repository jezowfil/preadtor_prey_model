using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class HuntField : MonoBehaviour
{
    public GameObject AnimalControl;
    private AnimalControl _animalControlScript;

    private GameObject _parent;
    private Prey _parentPreyScript;

    private SphereCollider _collider;

    public readonly HashSet<GameObject> victims = new();

    private void Start()
    {
        _parent = transform.parent.gameObject;
        _parentPreyScript = _parent.transform.GetComponent<Prey>();

        _animalControlScript = AnimalControl.GetComponent<AnimalControl>();
        if (TryGetComponent<SphereCollider>(out SphereCollider c))
        {
            _collider = c;
        }
        else
        {
            _collider = transform.gameObject.AddComponent<SphereCollider>();
            _collider.isTrigger = true;
        }

        if (!TryGetComponent<Rigidbody>(out Rigidbody rb))
        {
            rb = transform.gameObject.AddComponent<Rigidbody>();
        }
        rb.useGravity = false;
        rb.isKinematic = true;
    }

    private void Update()
    {

        _collider.radius = _animalControlScript.PredatorHuntRadius;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Prey"))
        {
            if (victims.Add(other.transform.gameObject))
            {
                Debug.Log($"Added {other.gameObject.name} to {_parent.name}'s predators.");

            }
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Prey"))
        {
            if (victims.Remove(other.transform.gameObject))
            {
                Debug.Log($"Removed {other.gameObject.name} from {_parent.name}'s predators.");

            }
        }
    }
}

