using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Prey : MonoBehaviour
{
    
    public GameObject pointA;
    public GameObject pointB;
    public GameObject neighField;
    public GameObject predField;
    public float rotSpdDegPerSec;

    public bool DrawSep;
    public bool DrawCoh;
    public bool DrawAli;
    public bool DrawMovementDir;
    public bool DrawPredAvoid;

    private AnimalControl _animalControlScript;
    
    private HashSet<GameObject> _neighbours;
    private HashSet<Predator> _preds;
    

    private Vector3 _pointPos;
    private Vector3 _pointDestination;

    private Vector3 _pointA;
    private Vector3 _pointB;
    
    private Vector3 _vecToDestination;
    private Vector3 _vecFwd;
    private Vector3 _vecMovementDir;
    private Vector3 _vecPredAvoid;

    private float speed;
    private float _separation;
    private float _forwardLength;
    private float _predatorFearDistance;

    private bool _isAvoidingPred;

    public static class ReynoldForces
    {
        public static Vector3 Sep = Vector3.zero; 
        public static Vector3 Coh = Vector3.zero; 
        public static Vector3 Ali = Vector3.zero; 
    }

    public bool IsGoingToA { get; set; } = false;
    public bool IsGoingToB { get; set; } = false;



    private void Start()
    {
        _pointPos = transform.position;
        _pointA = pointA.transform.position;
        _pointB = pointB.transform.position;
        _pointDestination = IsGoingToA ? _pointA:_pointB;
        

        _forwardLength = transform.localScale.z;

        _neighbours = neighField.GetComponent<NeighField>().neighbours;
        Debug.Assert(_neighbours != null);

        _preds = predField.GetComponent<PredField>().predators;
        Debug.Assert(_preds != null);

        _animalControlScript = GameObject.Find("AnimalControl").GetComponent<AnimalControl>();
        Debug.Assert(_animalControlScript != null);

    }

    private void FixedUpdate()
    {
        _pointPos = transform.position;
        _vecFwd = transform.forward;
        
        _pointDestination = IsGoingToA ? _pointA : _pointB;
        if(IsGoingToA && _pointDestination.Equals(_pointB)
            || IsGoingToB && _pointDestination.Equals(_pointA))
        {
            Debug.LogError("FUCK");
        }


        _vecToDestination = (_pointDestination - _pointPos).normalized;

        _isAvoidingPred = _preds.Count > 0;
        
        ComputeReynoldForces(); 
        ComputePredAvoidVec();

        _vecMovementDir = GetResultMovement();
        
        RotateEntity(_vecMovementDir);
        MoveEntity();
        
        DebugDraw();    
    }
    private void Update()
    {
        _separation = _animalControlScript.PreySeparation;
        speed = _animalControlScript.PreySpeed;
        _predatorFearDistance = _animalControlScript.PreyPredatorFearRadius;
        rotSpdDegPerSec = _animalControlScript.PreyRotSpd;
    }

    private void OnTriggerEnter(Collider other)
    {
        
        if (other.CompareTag("PointA"))
        {     
            IsGoingToB = true;
            IsGoingToA = false;
        }
        else if(other.CompareTag("PointB"))
        {
            
            IsGoingToA = true;
            IsGoingToB= false;
        }
        else if (other.CompareTag("Prey"))
        {
            Debug.LogWarning("Critical Intersection");
        }
    }


    private void RotateEntity(Vector3 lookTo)
    {
        Quaternion targetRotation = Quaternion.LookRotation(lookTo);
        float rotSpd = _isAvoidingPred ? rotSpdDegPerSec * 4 : rotSpdDegPerSec;
        transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, Time.fixedDeltaTime*rotSpd);

    }

    private void MoveEntity()
    {
        float spd = _isAvoidingPred ? speed * 2 : speed;
        transform.position = _pointPos + spd * Time.fixedDeltaTime * _vecFwd;
    }
    // SocialEntity
    private void ComputeReynoldForces()
    {
        ReynoldForces.Sep = Vector3.zero;
        ReynoldForces.Coh = Vector3.zero;
        ReynoldForces.Ali = Vector3.zero;

        foreach(var n in _neighbours)
        {
            float distance = Vector3.Distance(_pointPos, n.transform.position);
            Vector3 vecToNeigh = (n.transform.position - _pointPos).normalized;
            if (!_isAvoidingPred)
            {
                if(distance > _separation)
                {
                    ReynoldForces.Coh += vecToNeigh;
                    ReynoldForces.Ali += n.transform.forward;
                }
                else
                {
                    float power = _separation / distance;
                    ReynoldForces.Sep += - power * vecToNeigh;
                }
            }
            else
            {
                float minSeparation = _forwardLength*2;
                if (distance > minSeparation)
                {
                    ReynoldForces.Coh += vecToNeigh;
                    ReynoldForces.Ali += n.transform.forward;
                }
                else
                {
                    float power = minSeparation / distance;
                    ReynoldForces.Sep += -power * vecToNeigh;
                }
            }

            ReynoldForces.Sep.Normalize();
            ReynoldForces.Ali.Normalize();
            ReynoldForces.Coh.Normalize();
        }
    }

    private float GetPowerOfVector(in Vector3 leadDirection, in Vector3 directionToAdd)
    {
        float angleRad = Mathf.Deg2Rad * Vector3.Angle(leadDirection, directionToAdd);
        float ret = Mathf.Cos(angleRad)/2 + .5f;
        return ret;
    }

    private Vector3 GetResultMovement()
    {

        // Redo with priority decisions whne being pursued
        Vector3 result;
        if (_isAvoidingPred)
        {
            result = _vecPredAvoid;
            result += GetPowerOfVector(in result, in ReynoldForces.Coh) * ReynoldForces.Coh;
            result += GetPowerOfVector(in result, in ReynoldForces.Sep) * ReynoldForces.Sep;
            result += GetPowerOfVector(in result, in _vecToDestination) * _vecToDestination;

        }
        else
        {
            result = ReynoldForces.Sep;
            result += GetPowerOfVector(in result, in _vecToDestination) * _vecToDestination;
            result += GetPowerOfVector(in result, in ReynoldForces.Coh) * ReynoldForces.Coh;
        }
        /*
                result = ReynoldForces.Sep;
                result += GetPowerOfVector(in result, in _vecToDestination) * _vecToDestination;
                result += GetPowerOfVector(in result, in ReynoldForces.Coh) * ReynoldForces.Coh;*/


        return result.normalized;
    }

    // Prey
    private void ComputePredAvoidVec()
    {
        _vecPredAvoid = Vector3.zero;
        Vector3 vecAwayFromPred;
        float distance;
        foreach (var p in _preds) 
        {
            vecAwayFromPred = (_pointPos - p.transform.position);
            distance = vecAwayFromPred.magnitude;  
            if(distance < _predatorFearDistance)
            {

                _vecPredAvoid += (_predatorFearDistance / distance) *  vecAwayFromPred.normalized; 
            }
   
        }
        _vecPredAvoid.Normalize();
    }


    private void DebugDraw()
    {
        float l = 5f;
        if (DrawMovementDir)
            Debug.DrawLine(_pointPos, _pointPos + l * _vecMovementDir, Color.white);
        if(DrawCoh)
            Debug.DrawLine(_pointPos, _pointPos + l * ReynoldForces.Coh, Color.green);
        if(DrawAli)
            Debug.DrawLine(_pointPos, _pointPos + l * ReynoldForces.Ali, Color.blue);
        if(DrawSep)    
            Debug.DrawLine(_pointPos, _pointPos + l * ReynoldForces.Sep, Color.grey);
        if (DrawPredAvoid)
            Debug.DrawLine(_pointPos, _pointPos + l * _vecPredAvoid, Color.red);
    }
}
